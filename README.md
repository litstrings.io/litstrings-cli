LitStrings Command-Line Interface
===========================
[![PyPI version](https://badge.fury.io/py/litstrings-cli.svg)](https://badge.fury.io/py/litstrings-cli)



## Getting started

Description
---
The LitStrings CLI enables you to manage your translations within a project without the need of an elaborate UI system.

You can use the command line tool to create new resources, map locale files to translations, and synchronize your LitStrings project with your local repository. Translators and localization managers can use it to handle large volumes of translation files.  The LitStrings CLI can help to enable continuous integration workflows and can be run from CI servers like [RapidRabbit](https://rapidrabbit.iridiumintel.com), Jenkins and Bamboo.

[Click  here](http://docs.litstrings.info/cli/) for complete documentation on the LitStrings CLI via our documentation site.

Installation
------------

You can install the latest version of litstrings-cli running `pip install litstrings-cli` or `easy_install litstrings-cli`.


Getting Help
---
You can always get additional help via [GitLab issues](https://gitlab.com/litstrings.io/litstrings-cli/-/issues) or [LitStrings support](https://litstrings.info/#contact-section)

License
---
LitStrings CLI is primarily distributed under the terms of the GPL License (Version 2.0).

See [LICENSE](https://gitlab.com/litstrings.io/litstrings-cli/blob/master/LICENSE) for details.
